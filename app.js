console.log("Hello world!");

var bool = Boolean(true);
var x = null;
var text = "hello";
var num = 123;

console.log(bool);
console.log(x);
console.log(text);
console.log(num);

var i = 0;

while (i < 6) {
    j = 0;
    str = "";
    while (j < i) {
        str = str + "*";
        j++;
    }
    console.log(str);
    i++
}

for(let i=0; i <6; i++){
    str = "";
    for(let j=0; j < i; j++) {
        str = str + "*";
    }
    console.log(str);
}

function distance(x1, x2, y1, y2) {
    return Math.sqrt((x2 - x1)**2 + (y2 - y1)**2);
}

console.log(distance(5,8,6,9));
console.log(distance(0,3,0,4));

const point_a = {
    x: 5,
    y: 6,
}

const point_b = {
    x: 8,
    y: 9,
}

const point_c = {
    x: 0,
    y: 0,
}

const point_d = {
    x: 3,
    y: 4,
}


function distanceobj(point1, point2) {
    return Math.sqrt((point2.x - point1.x)**2 + (point2.y - point1.y)**2);
}

console.log(distanceobj(point_a, point_b));
console.log(distanceobj(point_c, point_d));

const numberAndSquare = [];


for (let i =2 ; i < 12; i++){
    numberAndSquare.push({'num': i, 'square': i**2})
}

function printAllInArray(arr){
    for (let i =0 ; i < arr.length; i++){
        console.log(arr[i]);
    }
}

printAllInArray(numberAndSquare);
